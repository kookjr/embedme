/*
 * Copyright (c) 2010, 2012 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EMB_ATOM_H
#define EMB_ATOM_H

#define EMB_ATOM_FLAG_ALLOC    0x0001

#define EMB_ATOM_TYPE_LIST     0
#define EMB_ATOM_TYPE_FUNCTION 1
#define EMB_ATOM_TYPE_INTEGER  2
#define EMB_ATOM_TYPE_FLOAT    3
#define EMB_ATOM_TYPE_SYMBOL   4
#define EMB_ATOM_TYPE_BOOL     5
#define EMB_ATOM_TYPE_VOIDP    6
#define EMB_ATOM_TYPE_LAST     7  /* must be one past last type */

typedef struct emb_atom {
    struct emb_atom* a_link;
    unsigned short   a_flags;
    unsigned short   a_type;
    union {
        short        a_symbol;
        int          a_int;
        double       a_float;
        void*        a_voidp;
        struct emb_atom* a_list;
    } a_val;
} emb_atom_t;

#ifdef __cplusplus
extern "C" {
#endif

extern emb_atom_t* emb_alloc_atom(void);
extern void emb_release_atom(emb_atom_t* atom);

extern char emb_atom_types[];

#ifdef __cplusplus
}
#endif

#endif /* EMB_ATOM_H */
