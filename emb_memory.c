/*
 * Copyright (c) 2010 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <assert.h>
#include "emb_memory.h"

struct mem_item {
    struct mem_item* next;
    void* item;
};

struct mem_head {
    struct mem_item* head;
    struct mem_item* tail;
};

static struct mem_head pools[EMB_MEM_POOL_TOTAL];

void emb_pool_add_item(int mem_pool, void* item) {
    struct mem_head* h;
    struct mem_item* it;

    assert(mem_pool < EMB_MEM_POOL_TOTAL);
    /* creat new item */
    it = (struct mem_item* )malloc(sizeof(struct mem_item));
    it->item = item;
    it->next = NULL;
    /* link it in */
    h = &pools[mem_pool];
    if (h->head == NULL) {
        h->head = it;
    }
    else {
        h->tail->next = it;
    }
    h->tail = it;
}

void emb_pool_release_all_items(int mem_pool) {
    struct mem_head* h;
    struct mem_item* it;
    struct mem_item* tmp;

    assert(mem_pool < EMB_MEM_POOL_TOTAL);
    h = &pools[mem_pool];
    it = h->head;

    while (it) {
        assert(it->item);
        free(it->item);  /* free item's data */
        tmp = it;
        it = it->next;   /* move to next */
        free(tmp);       /* free item holder */
    }

    h->tail = h->head = NULL;
}
