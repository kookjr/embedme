# Copyright (c) 2010, 2012, 2018 Mathew Cucuzella (kookjr@gmail.com)
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#    3. The name of the author may not be used to endorse or promote
#       products derived from this software without specific prior written
#       permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

CC      = gcc
AR      = ar

# install base
PREFIX  = /usr/local

# Command line option to control build:
#   -DEMB_DEBUG
#      Add debugging symbols to build, otherwise default to
#      optimized stripped build
#   -DEMB_PEVAL
#      Print evaluation debugging
ifdef EMB_DEBUG
  CFLAGS  = -g
  LDFLAGS = -g
else
  CFLAGS  = -O2 -s -DNDEBUG
  LDFLAGS = -s
endif
ifdef EMB_PEVAL
  CFLAGS  += -DPRINT_EVAL
endif

SRC = main.c emb_parser.c emb_functions.c emb_symbol.c emb_atom.c emb_memory.c

APIHDR  = emb_atom.h emb_functions.h emb_parser.h

LIBSRC := $(filter-out main.c, $(SRC))
LIBOBJ := $(LIBSRC:.c=.o)

LIBRARY := libemb.a

.PHONY: clean clobber install uninstall

$(LIBRARY): $(LIBOBJ)
	$(AR) rv $(LIBRARY) $(LIBOBJ)

t: main.o $(LIBRARY)
	$(CC) $(LDFLAGS) -o t main.o $(LIBRARY)

dev:
	$(MAKE) EMB_DEBUG=1 EMB_PEVAL=1

dev-t:
	$(MAKE) EMB_DEBUG=1 EMB_PEVAL=1 t

install: $(LIBRARY)
	install -m 0755 -d $(PREFIX)/lib
	install -m 0755 -d $(PREFIX)/include
	install -m 0644 $(LIBRARY) $(PREFIX)/lib
	install -m 0644 $(APIHDR)  $(PREFIX)/include

uninstall:
	rm -f $(PREFIX)/lib/$(LIBRARY)
	for file in $(APIHDR); do \
		rm -f $(PREFIX)/include/$$file; \
	done

clean:
	rm -f main.o $(LIBOBJ) $(LIBRARY)

clobber: clean
	rm -f t *.d


# --- auto dependency files
-include $(subst .c,.d,$(SRC))

%.d: %.c
	$(CC) -M $(CPPFLAGS) $< > $@.$$$$;                      \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@;     \
	rm -f $@.$$$$
