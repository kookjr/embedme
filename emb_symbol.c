/*
 * Copyright (c) 2010 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "emb_symbol.h"

#define NSYMBOLS 100

static char* symbol_table[NSYMBOLS];

int emb_lookup_or_add_symbol(const char* name) {
    int i;

    for (i=0; i < NSYMBOLS && symbol_table[i] != NULL; i++) {
        if (strcmp(name, symbol_table[i]) == 0) {
            return i;
        }
    }
    assert(i < (NSYMBOLS-1));
    /* did not find, add new one at end */
    symbol_table[i] = strdup(name);

    return i;
}

const char* emb_symbol_name_by_id(int id) {
    assert(id < NSYMBOLS);
    return symbol_table[id];
}

void emb_release_symbols(void) {
    int i;

    for (i=0; i < NSYMBOLS && symbol_table[i] != NULL; i++) {
        free(symbol_table[i]);
        symbol_table[i] = NULL;
    }
}
