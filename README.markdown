[![Build Status](https://travis-ci.org/kookjr/embedme.svg?branch=master)](https://travis-ci.org/kookjr/embedme)

# embedme

embedme -- a super tiny framework for embedding in a C/C++ program that adds dynamic expression
evaluation with a LISP-like syntax.

Above all, this framework is meant to be very small, around 10K. It is meant to be compiled
into other programs to provide a dynamic evaluation environment. The integrations steps
are expected to be:

* build into your program
* add bindings to functions that are needed from the main program
* pass evaluation code (as a string) and get response

# Development
## Source Repository

*embedme* is currently hosted at gitlab. The gitlab web page is
``https://gitlab.com/kookjr/embedme``. The public git clone URL is

* ``https://gitlab.com/kookjr/embedme.git``

## Build Source

A test can be built with the provided makefile, and a simple test demonstrates
the use.

    make t
    ./t

To add debugging use the following optional command line arguments to make.
* ``EMB_DEBUG=1`` - Add debugging symbols (``-g``)
* ``EMB_PEVAL=1`` - Print evaluation debugging

Two simple targes are provided for debug builds
* ``dev`` - build library with both debug flags set above
* ``dev-t`` - build test and library with both debug flags set above

To install a library, ``libemb.a``, and header files in ``/usr/local``:

    sudo make install

To install in somewhere in your home directory, use:

    make install PREFIX=/home/user/somedir

## Sample Code

Look at ``main.c`` for sample code and several examples. The example below
shows evaluation of one script.

```C
    emb_parser_data_t* pd;
    int script_result;

    /* allocate data/handle for the parser */
    pd = alloc_emb_parser_data();
    if (pd == NULL) {
        printf("allocation of parser data failed\n");
        return 1;
    }

    /* evaluate a simple script */
    if (! emb_evaluate_ret_int(pd, &script_result, "(+ 3 7)")) {
        printf("script execution failed\n");
    }
    else {
        printf("3 + 7 = %d\n", script_result);
    }

    /* cleanup */
    free_emb_parser_data(pd);
```

## Considerations

The following rules must be observed for proper behavior of this library.
* Each call to ``alloc_emb_parser_data`` must be matched with a call
to ``free_emb_parser_data``. Multiple pairs of these calls can be made
in a single program.
* Multiple ``emb_evaluate_*`` calls can be made serially between the above
calls.
* ``emb_evaluate_*`` calls can not be made from different threads concurrently
because the library uses static data.

# Memory Usage

``embedme`` uses dynamic memory allocated by malloc(3). The function ``alloc_emb_parser_data``
allocates two buffers. These are freed in the call to ``free_emb_parser_data``.

During evaluation, for each token in the evaluation script two buffers are allocated.
Finally one buffer is allocated for each unique symbol name (the function of a list).
These are freed at the end of the evaluation, just before any form of ``emb_evaluate`` returns.

    Example 1: (+ 1 2)
    Example 2: (+ 1 (+ 3 4))

For the first example the number of dynamic allocations is; 2 + 2 * 3 + 1 = 9.

For the second example; 2 + 2 * 6 + 1 = 15. Note that although the symbol + occurs twice, there is
only one buffer allocated. Also the expression ``(+ 3 4)`` has three tokens when it's
evaluate as a sub-expression and returns one for the calling expression, so this counts as three
and one.

# License

BSD-style license. For details, see "COPYING" file.
