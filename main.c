/*
 * Copyright (c) 2010, 2017 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include "emb_atom.h"
#include "emb_parser.h"
#include "emb_functions.h"

emb_atom_t* f_frogs_are_land_animals(emb_atom_t* form);
emb_atom_t* f_random_string(emb_atom_t* form);
emb_atom_t* f_print_string(emb_atom_t* form);

static emb_function_t func_table[] = {
    "frogs-are-land-animals", "II", f_frogs_are_land_animals,
    "random-string",           "",  f_random_string,
    "print-string",            "V", f_print_string
};

int main() {
    emb_parser_data_t* pd;
    int result;
    double d_result;

    /* add our functions to the system */
    emb_append_functions(func_table, sizeof(func_table)/sizeof(emb_function_t));

    /* allocate data/handle for the parser */
    pd = alloc_emb_parser_data();
    if (pd == NULL) {
        printf("allocation of parser data failed\n");
        return 1;
    }

    int r = 1;

    /* evaluate some code */
    emb_evaluate_ret_bool(pd, &result, "(frogs-are-land-animals (+ 3 7) 1)");
    printf("frogs-are-land-animals = %s\n", result ? "true" : "false");
    if (result != 1)
        goto cleanup;

    emb_evaluate_ret_bool(pd, &result, "(print-string (random-string))");
    printf("print-string = %s\n", result ? "true" : "false");
    if (result != 1)
        goto cleanup;

    emb_evaluate_ret_int(pd, &result, "( +  #o10  #x1f )");
    printf("#o10 + #x1f = %d\n", result);
    if (result != 39)
        goto cleanup;

    emb_evaluate_ret_int(pd, &result, "(+  -3  8)");
    printf("-3 + 8 = %d\n", result);
    if (result != 5)
        goto cleanup;

    emb_evaluate_ret_double(pd, &d_result, "(f+ 3.0 7.3)");
    printf("3.0 + 7.3 = %f\n", d_result);
    if (d_result != 10.3)
        goto cleanup;

    emb_evaluate_ret_bool(pd, &result, "(< 3 7)");
    printf("3 < 7 = %s\n", result ? "true" : "false");
    if (! result)
        goto cleanup;

    r = 0;

cleanup:
    /* cleanup */
    free_emb_parser_data(pd);

    return r;
}

emb_atom_t* f_frogs_are_land_animals(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("frogs_are_land_animals", args);
    /* TODO fake data for now */
    atom->a_type = EMB_ATOM_TYPE_BOOL;
    atom->a_val.a_int = 1;
    return atom;
}
emb_atom_t* f_random_string(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("random-string", args);
    atom->a_type = EMB_ATOM_TYPE_VOIDP;
    atom->a_val.a_voidp = (void* )"some string\n";
    return atom;
}
emb_atom_t* f_print_string(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("print-string", args);

    printf("%s", (char* )args->a_val.a_voidp);
    atom->a_type = EMB_ATOM_TYPE_BOOL;
    atom->a_val.a_int = 1;
    return atom;
}
