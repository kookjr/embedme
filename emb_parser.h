/*
 * Copyright (c) 2010, 2012 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EMB_PARSER_H
#define EMB_PARSER_H

typedef struct emb_parser_data {
    char* token;            /* buffer used to collect a token */
    char* ptok;             /* current position in token */
    const char* expression; /* full expression being emb_evaluated */
    const char* pexpr;      /* current position in expression */
} emb_parser_data_t;

#ifdef __cplusplus
extern "C" {
#endif

/* API functions */
extern emb_parser_data_t* alloc_emb_parser_data();
extern void free_emb_parser_data(emb_parser_data_t* pdata);

extern int emb_evaluate_ret_int   (emb_parser_data_t* pdata, int* retval,    const char* eval_str);
extern int emb_evaluate_ret_double(emb_parser_data_t* pdata, double* retval, const char* eval_str);

#define emb_evaluate          emb_evaluate_ret_int
#define emb_evaluate_ret_bool emb_evaluate_ret_int

#ifdef __cplusplus
}
#endif

#endif /* EMB_PARSER_H */
