/*
 * Copyright (c) 2010, 2012, 2018 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "emb_atom.h"
#include "emb_symbol.h"
#include "emb_parser.h"
#include "emb_functions.h"
#include "emb_memory.h"

#include <setjmp.h>

/* max list size which can be parsed */
#define TOKEN_SIZE 128

/* internal: supported return types */
#define EVAL_RETURN_TYPE_INT     0
#define EVAL_RETURN_TYPE_DOUBLE  1

static jmp_buf non_local_jump_buffer;

void fail_and_terminate_eval(int reason) {
    longjmp(non_local_jump_buffer, reason);
}

static emb_atom_t* token2atom(emb_parser_data_t* pdata) {
    emb_atom_t* atom;
    atom = emb_alloc_atom();

    /* need to handle sign TODO */
    /* go through the token and determine some attributes on the fly */
    int digits = 0;
    int dots   = 0;
    int length = 0;
    int radix  = 10;
    int sign   = 1;

    char* p    = pdata->token;
    char* intp = pdata->token;

    if (*p == '-') {
        sign = -1;
        p   += 1;
        intp = p;
    }
    else if (*p == '#' && (*(p+1) == 'x' || *(p+1) == 'o')) {
        radix = ( *(p + 1) == 'x' ) ? 16 : 8;
        p    += 2;
        intp  = p;
    }

    while (*p) {
        length++;
        if (*p >= '0' &&
            (radix == 10 && (*p <= '9')) ||
            (radix ==  8 && (*p <= '7')) ||
            (radix == 16 && isxdigit(*p))) {
            digits++;
        }
        else if (*p == '.') {
            dots++;
        }
        p++;
    }
    if (length == 0)
        fail_and_terminate_eval(1); /* found unexpected character */

    /* determint type */
    if (digits == length) {
        atom->a_type = EMB_ATOM_TYPE_INTEGER;
        atom->a_val.a_int = strtol(intp, NULL, radix) * sign;
    }
    else if (digits == (length-1) && dots == 1) {
        atom->a_type = EMB_ATOM_TYPE_FLOAT;
        atom->a_val.a_float = atof(intp) * sign;
    }
    else if (strcmp(pdata->token, "nil") == 0) {
        atom->a_type = EMB_ATOM_TYPE_BOOL;
        atom->a_val.a_int = 0;
    }
    else if (strcmp(pdata->token, "t") == 0) {
        atom->a_type = EMB_ATOM_TYPE_BOOL;
        atom->a_val.a_int = 1;
    }
    else {
        atom->a_type = EMB_ATOM_TYPE_SYMBOL;
        atom->a_val.a_symbol = emb_lookup_or_add_symbol(pdata->token);
    }

    return atom;
}

/* if one exitsts */
static emb_atom_t* process_token(emb_parser_data_t* pdata) {
    emb_atom_t* atom = NULL;

    if (pdata->ptok != pdata->token) {
        atom = token2atom(pdata);
        pdata->ptok = pdata->token;
        *pdata->ptok = '\0';
    }
    return atom;
}

static int next_char(emb_parser_data_t* pdata) {
    int rval = -1;

    if (*pdata->pexpr != '\0') {
        rval = *(pdata->pexpr++);
    }
    return rval;
}

#define LINK_ATOM(addp, listp, endp)      \
                if (endp == NULL) {       \
                    listp = addp;         \
                }                         \
                else {                    \
                    endp->a_link = addp;  \
                }                         \
                endp = addp;

static emb_atom_t* r_emb_evaluate(emb_parser_data_t* pdata) {
    emb_atom_t* c_list = NULL;
    emb_atom_t* c_atom = NULL;
    emb_atom_t* ta;
    int     c;

    while ( (c=next_char(pdata)) != -1 ) {
        switch ((char )c) {
        case '(':
            /* process previous token and add atom to list */
            if ( (ta=process_token(pdata)) != NULL ) {
                LINK_ATOM(ta, c_list, c_atom);
            }
            /* recursive call to eval embedded expression */
            /*   add atom of emb_evaluated list to current list */
            ta = r_emb_evaluate(pdata);
            LINK_ATOM(ta, c_list, c_atom);
            break;
        case ')':
            /* process previous token and add atom to list */
            if ( (ta=process_token(pdata)) != NULL ) {
                LINK_ATOM(ta, c_list, c_atom);
            }
            ta = emb_execute_form(c_list);
            return ta;
            break;
        case ' ':
        case '\t':
        case '\n':
        case '\r':
            /* process previous token and add atom to list */
            if ( (ta=process_token(pdata)) != NULL ) {
                LINK_ATOM(ta, c_list, c_atom);
            }
            break;
        default:
            /* collect chars for current token */
            *(pdata->ptok++) = (char )c;
            *pdata->ptok = '\0';
            break;
        }
    }

    return c_atom;
}

emb_parser_data_t* alloc_emb_parser_data() {
    emb_parser_data_t* pd = (emb_parser_data_t* )malloc(sizeof(emb_parser_data_t));
    if (pd) {
        pd->token = (char* )malloc(TOKEN_SIZE);
        if (pd->token == NULL) {
            free_emb_parser_data(pd);
            pd = NULL;
        }
    }
    return pd;
}

void free_emb_parser_data(emb_parser_data_t* pdata) {
    if (pdata) {
        if (pdata->token) {
            free(pdata->token);
        }
        free(pdata);
    }
}

static int emb_evaluate_any(emb_parser_data_t* pdata, void* retval, int rettype, const char* eval_str) {
    if (pdata == NULL || retval == NULL || eval_str == NULL || pdata->token == NULL)
        return 0;

    int func_ret = 1;

    int jmp_ret = setjmp(non_local_jump_buffer);

    if (jmp_ret == 0) {
        pdata->expression = eval_str;
        pdata->pexpr = eval_str;
        pdata->ptok = pdata->token;
        *pdata->ptok = '\0';

        emb_atom_t* atom = r_emb_evaluate(pdata);

        if (retval != NULL) {
            switch (rettype) {
            case EVAL_RETURN_TYPE_INT:
                *((int* )retval) = atom->a_val.a_int;
                break;
            case EVAL_RETURN_TYPE_DOUBLE:
                *((double* )retval) = atom->a_val.a_float;
                break;
            default:
                func_ret = 0;
                break;
            }
        }
    }
    else {
        func_ret = 0;
    }

    /* garbage collect memory */
    emb_release_symbols();
    emb_pool_release_all_items(EMB_MEM_POOL_ATOMS);

    return func_ret;
}

int emb_evaluate_ret_int(emb_parser_data_t* pdata, int* retval, const char* eval_str) {
    return emb_evaluate_any(pdata, retval, EVAL_RETURN_TYPE_INT, eval_str);
}

int emb_evaluate_ret_double(emb_parser_data_t* pdata, double* retval, const char* eval_str) {
    return emb_evaluate_any(pdata, retval, EVAL_RETURN_TYPE_DOUBLE, eval_str);
}
