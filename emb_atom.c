/*
 * Copyright (c) 2010 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include "emb_memory.h"
#include "emb_atom.h"

char emb_atom_types[EMB_ATOM_TYPE_LAST] = {
    'L',  /* EMB_ATOM_TYPE_LIST */
    'f',  /* EMB_ATOM_TYPE_FUNCTION */
    'I',  /* EMB_ATOM_TYPE_INTEGER */
    'F',  /* EMB_ATOM_TYPE_FLOAT */
    'S',  /* EMB_ATOM_TYPE_SYMBOL */
    'B',  /* EMB_ATOM_TYPE_BOOL */
    'V'   /* EMB_ATOM_TYPE_VOIDP */
};

emb_atom_t* emb_alloc_atom(void) {
    emb_atom_t* atom;

    atom = (emb_atom_t* )malloc(sizeof(emb_atom_t));
    atom->a_link  = NULL;
    atom->a_flags = EMB_ATOM_FLAG_ALLOC;
    atom->a_type  = EMB_ATOM_TYPE_INTEGER;
    atom->a_link  = NULL;

    emb_pool_add_item(EMB_MEM_POOL_ATOMS, atom);

    return atom;
}

void emb_release_atom(emb_atom_t* atom) {
    atom->a_flags &= ~EMB_ATOM_FLAG_ALLOC;
}
