/*
 * Copyright (c) 2010, 2012, 2018 Mathew Cucuzella (kookjr@gmail.com)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *    3. The name of the author may not be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include "emb_functions.h"
#include "emb_symbol.h"
#include "emb_atom.h"

#ifdef PRINT_EVAL
#include <stdio.h>
#endif

/* Functions table */
emb_atom_t* f_list(emb_atom_t* form);
emb_atom_t* f_less_than(emb_atom_t* form);
emb_atom_t* f_less_than_float(emb_atom_t* form);
emb_atom_t* f_plus(emb_atom_t* form);
emb_atom_t* f_plus_float(emb_atom_t* form);
emb_atom_t* f_bitwise_and(emb_atom_t* form);
emb_atom_t* f_bitwise_or(emb_atom_t* form);
emb_atom_t* f_bitwise_xor(emb_atom_t* form);

static emb_function_t func_table[] = {
    "list", "*", f_list,

    "<", "II", f_less_than,
    "+", "II", f_plus,

    "f<", "FF", f_less_than_float,
    "f+", "FF", f_plus_float,

    "and", "II", f_bitwise_and,
    "or",  "II", f_bitwise_or,
    "xor", "II", f_bitwise_xor
};

extern void fail_and_terminate_eval(int reason);

static emb_function_t* extra_table;
static int extra_table_size;

void emb_append_functions(emb_function_t* extra, int size) {
    extra_table = extra;
    extra_table_size = size;
}

static emb_function_t* find_function(const char* name) {
    int i;
    emb_function_t* func = NULL;
    for (i=0; i < sizeof(func_table)/sizeof(emb_function_t); i++) {
        if (strcmp(name, func_table[i].name) == 0) {
            func = &func_table[i];
            break;
        }
    }
    if (func == NULL && extra_table != NULL) {
        for (i=0; i < extra_table_size; i++) {
            if (strcmp(name, extra_table[i].name) == 0) {
                func = &extra_table[i];
                break;
            }
        }
    }
    return func;
}

emb_atom_t* check_signature(emb_function_t* func, emb_atom_t* args) {
    const char* p;
    emb_atom_t* tmp;

    tmp = args;
    p   = func->signature;
    if (*p != '*') {
        /* check signature against actual args */
        while ( *p ) {
            if (tmp == NULL || emb_atom_types[tmp->a_type] != *p)  /* case of wrong arg type */
                fail_and_terminate_eval(1);
            tmp = tmp->a_link;
            p++;
        }
        if (tmp != NULL) /* case of extra args */
            fail_and_terminate_eval(1);
    }
    /* all OK, execute function */
    return func->func(args);
}

/* API functions */

emb_atom_t* emb_execute_form(emb_atom_t* form) {
    const char* symname;
    emb_function_t* func;

    if (form->a_type != EMB_ATOM_TYPE_SYMBOL)
        fail_and_terminate_eval(1);

    symname = emb_symbol_name_by_id(form->a_val.a_symbol);
    func = find_function(symname);

    if (! func)
        fail_and_terminate_eval(1);

    return check_signature(func, form->a_link);
}

static void print_atom(emb_atom_t* a) {
#ifdef PRINT_EVAL
    switch (a->a_type) {
    case EMB_ATOM_TYPE_LIST:
        printf(" list");
        break;
    case EMB_ATOM_TYPE_FUNCTION:
        printf(" func");
        break;
    case EMB_ATOM_TYPE_INTEGER:
        printf(" %d", a->a_val.a_int);
        break;
    case EMB_ATOM_TYPE_FLOAT:
        printf(" %f", a->a_val.a_float);
        break;
    case EMB_ATOM_TYPE_SYMBOL:
        printf(" sym");
        break;
    case EMB_ATOM_TYPE_BOOL:
        printf(" %s", a->a_val.a_int ? "true" : "false");
        break;
    case EMB_ATOM_TYPE_VOIDP:
        printf(" %lu", (unsigned long)a->a_val.a_voidp);
        break;
    default:
        printf(" unkn");
        break;
    }
#endif
}

void emb_print_func(const char* func_name, emb_atom_t* list) {
#ifdef PRINT_EVAL
    emb_atom_t* a = list;

    printf("evaluating: %s", func_name);
    while (a) {
        print_atom(a);
        a = a->a_link;
    }
    printf("\n");
#endif
}
static emb_atom_t* build_int_atom(int val) {
    emb_atom_t* atom = emb_alloc_atom();
    atom->a_type = EMB_ATOM_TYPE_INTEGER;
    atom->a_val.a_int = val;
    return atom;
}
/* Built-in functions */
emb_atom_t* f_list(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();
    emb_print_func("list", args);
    atom->a_type = EMB_ATOM_TYPE_LIST;
    atom->a_val.a_list = args;
    return atom;
}
emb_atom_t* f_less_than(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("<", args);
    atom->a_type = EMB_ATOM_TYPE_BOOL;
    atom->a_val.a_int = (args->a_val.a_int < args->a_link->a_val.a_int) ? 1 : 0;
    return atom;
}
emb_atom_t* f_less_than_float(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("f<", args);
    atom->a_type = EMB_ATOM_TYPE_BOOL;
    atom->a_val.a_int = (args->a_val.a_float < args->a_link->a_val.a_float) ? 1 : 0;
    return atom;
}
emb_atom_t* f_plus(emb_atom_t* form) {
    emb_print_func("+", form);
    return build_int_atom(form->a_val.a_int + form->a_link->a_val.a_int);
}
emb_atom_t* f_plus_float(emb_atom_t* args) {
    emb_atom_t* atom = emb_alloc_atom();

    emb_print_func("f+", args);
    atom->a_type = EMB_ATOM_TYPE_FLOAT;
    atom->a_val.a_float = args->a_val.a_float + args->a_link->a_val.a_float;
    return atom;
}
emb_atom_t* f_bitwise_and(emb_atom_t* form) {
    emb_print_func("and", form);
    return build_int_atom(form->a_val.a_int & form->a_link->a_val.a_int);
}
emb_atom_t* f_bitwise_or(emb_atom_t* form) {
    emb_print_func("or", form);
    return build_int_atom(form->a_val.a_int | form->a_link->a_val.a_int);
}
emb_atom_t* f_bitwise_xor(emb_atom_t* form) {
    emb_print_func("xor", form);
    return build_int_atom(form->a_val.a_int ^ form->a_link->a_val.a_int);
}
